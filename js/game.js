// REFACTORING
// GAME
// Create the game board, set properties, add to body
    // The game board is simply a canvas element
    var gameBoard = document.createElement("canvas");
    gameBoard.width = 512;
    gameBoard.height = 480;
    gameBoard.style.background = "lightBlue";
    document.body.appendChild(gameBoard);

    // Get game board context
    var gameBoardContext = gameBoard.getContext("2d");

    gameBoardContext.font = "20px Georgia";
    var allImagesReady = false;

    function allImagesLoaded() {
        allImagesReady = allImagesReady || (jacobBadReady && jacobGoodReady && jacobMediumReady && broccoliReady && powerup_ready);
        return allImagesReady;
    }

    var scoreboard = {
        positiveScore: 0,
        positiveScoreCoordinates: { x: 100, y: 75 },

        negativeScore: 0,
        negativeScoreCoordinates: { x: 100, y: 50 },

        speed: 0,

        render: function(){
            gameBoardContext.fillText("Veggies eaten: " + this.negativeScore, this.negativeScoreCoordinates.x, this.negativeScoreCoordinates.y);
            gameBoardContext.fillText("Candies found: " + this.positiveScore, this.positiveScoreCoordinates.x, this.positiveScoreCoordinates.y);
            gameBoardContext.fillText("Speed: " + this.speed, 100, 100);
        },

	    update: function(num_powerups_touched, num_enemies_touched, hero_speed){

        		// update scoreboard
        		this.positiveScore = num_powerups_touched;
        		this.negativeScore = num_enemies_touched;
        		this.speed = hero_speed;
        		// render scoreboard
       		this.render();
        }
    };

// CONTROLLER
    // Handle keyboard controls
    var keysDown = {};

    addEventListener("keydown", function (e) {
        keysDown[e.keyCode] = true;
    }, false);

    addEventListener("keyup", function (e) {
        delete keysDown[e.keyCode];
        keyPressed = "";
    }, false);

// HERO
    //// Jacob Medium image
    var jacobMediumReady = false;
    var jacobMediumImage = new Image();
    jacobMediumImage.onload = function () {
        jacobMediumReady = true;
    };
    jacobMediumImage.src = "images/jacobGameFaceMedium.jpg";

    //// Jacob good image
    var jacobGoodReady = false;
    var jacobGoodImage = new Image();
    jacobGoodImage.onload = function () {
        jacobGoodReady = true;
    };
    jacobGoodImage.src = "images/jacobGameFaceGood.jpg";

    // Jacob bad image
    var jacobBadReady = false;
    var jacobBadImage = new Image();
    jacobBadImage.onload = function () {
        jacobBadReady = true;
    };
    jacobBadImage.src = "images/jacobGameFaceBad.jpg";
    var hero = {
        width: 50,
        height: 50,
        speed: 500,
        touchingEnemy: false,
        wasTouchingEnemy: false,
        touchingGoodItem: false,
        wasTouchingGoodItem: false,
        x: 0,
        y: 0,
        boundByCanvas: true,
        image: jacobMediumImage,
        numEnemiesTouched: 0,
        numGoodItemsTouched: 0,
        updateCoordinates: function(timeDelta){
            function GetDeltaX(timeDelta) {

                var deltaX = 0;

                var timeDeltaInSeconds = timeDelta / 1000;
                var absoluteDeltaX = timeDeltaInSeconds * hero.speed;

                if (39 in keysDown) { // Player holding right
                    deltaX += absoluteDeltaX;
                }

                if (37 in keysDown) { // Player holding left
                    deltaX += (absoluteDeltaX * -1);
                }

                return deltaX;
            }
            
            function GetDeltaY(timeDelta) {

                var deltaY = 0;
                var timeDeltaInSeconds = timeDelta / 1000;
                var absoluteDeltaY = timeDeltaInSeconds * hero.speed;

                if (40 in keysDown) { // Player holding down
                    deltaY += absoluteDeltaY;
                }

                if (38 in keysDown) { // Player holding up
                    deltaY += (absoluteDeltaY * -1);
                }

                return deltaY;
            }

            // Calculate new hero coordinates
            hero.x += GetDeltaX(timeDelta);
            hero.y += GetDeltaY(timeDelta);

            // Makes character stay inside canvas borders
            if (hero.boundByCanvas) {
                if (hero.x < 0) { hero.x = 0; }
                if (hero.x > gameBoard.width - hero.width) { hero.x = gameBoard.width - hero.width; }
                if (hero.y > gameBoard.height - hero.height) { hero.y = gameBoard.height - hero.height; }
                if (hero.y < 0) { hero.y = 0; }
            }  
        }  
    }

    function handleTouchingEnemy() {

        hero.touchingEnemy = areTouching(hero, enemy);

        if (hero.touchingEnemy) {
            hero.image = jacobBadImage;

            if (!hero.wasTouchingEnemy) {
                hero.numEnemiesTouched++;
                hero.speed = Math.round(hero.speed * .75);
                hero.wasTouchingEnemy = true;
            }
        }
        else {

            if (hero.wasTouchingEnemy) {
                shuffleItems();
            }

            hero.wasTouchingEnemy = false;
        }
    }

    function handleTouchingGoodItem() {

        // Scenarios:
        // Touching Good Item Now = TB
        // 


        hero.touchingGoodItem = areTouching(hero, goodItem);


        if (hero.touchingGoodItem && !hero.wasTouchingGoodItem) {
            hero.numGoodItemsTouched++;
        }

        if (!hero.touchingEnemy && hero.touchingGoodItem) {
            hero.image = jacobGoodImage;
        }

        if (hero.wasTouchingGoodItem && !hero.touchingGoodItem) {
            shuffleItems();
            hero.speed = Math.round(hero.speed * 1.25);
        }

        hero.wasTouchingGoodItem = hero.touchingGoodItem;
    }

// ENEMY
    // broccoli image
    var broccoliReady = false;
    var broccoliImage = new Image();
    broccoliImage.onload = function () {
        broccoliReady = true;
    };
    broccoliImage.src = "images/broccoli.jpg";
    var enemy = {
    image: broccoliImage,
    x: 300,
    y: 300,
    width: 50,
    height: 50
    }

// POWERUP
    // skittle image
    var powerup_ready = false;
    var skittleImage = new Image();
    skittleImage.onload = function () {
        powerup_ready = true;
    };
    skittleImage.src = "images/skittle.jpg";
    var goodItem = {
        image: skittleImage,
        x: 150,
        y: 150,
        width: 50,
        height: 46
    }

function areTouching(tangible1, tangible2) {
    return tangible1.x >= tangible2.x - tangible1.width && tangible1.x <= tangible2.x + tangible2.width && tangible1.y + tangible1.height >= tangible2.y && tangible1.y <= tangible2.y + tangible2.height;
}

function renderScreen() {
    
    // Clear the canvas
    gameBoardContext.clearRect(0, 0, gameBoard.width, gameBoard.height);

    // Show 'loading' screen if all items not ready
    if (!allImagesLoaded()) {
        gameBoardContext.fillText("Loading", 100, 75);
        return;
    }

    // update scoreboard
    scoreboard.update(hero.numGoodItemsTouched, hero.numEnemiesTouched, hero.speed);

    // Draw enemy
    gameBoardContext.drawImage(enemy.image, enemy.x, enemy.y);

    // Draw powerup
    gameBoardContext.drawImage(goodItem.image, goodItem.x, goodItem.y);

    // Draw hero
    gameBoardContext.drawImage(hero.image, hero.x, hero.y);
}

function shuffleItems() {
    goodItem.x = 0 + (Math.random() * (gameBoard.width - goodItem.width));
    goodItem.y = 0 + (Math.random() * (gameBoard.height - goodItem.height));
    enemy.x = 0 + (Math.random() * (gameBoard.width - enemy.width));
    enemy.y = 0 + (Math.random() * (gameBoard.height - enemy.height));
}

function loop() {

    var startTimeThisLap = Date.now();
    var timeDelta = startTimeThisLap - startTimePreviousLap;

    // Update object states
    hero.updateCoordinates(timeDelta);
    handleTouchingEnemy();
    handleTouchingGoodItem();

    // Redraw the game board
    renderScreen();

    startTimePreviousLap = startTimeThisLap;
}

// 2)
var startTimePreviousLap = Date.now();

// 1)
// only line that is actually executing on load.
// sets the loop function to repeat every 1 millisecond
setInterval(loop, 1);
